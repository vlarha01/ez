# README #

Se requiere una aplicación para contabilizar el tiempo de parada de las máquinas que permita obtener los registros de parada. La aplicación debe funcionar en Android y Windows. En el background debe haber una base de datos unificada para todas las líneas que contabilice el tiempo de las paradas por cada línea y por causal. Debe tener una estructura similar a esto: Línea, Fecha, Hora parada, causal, duración de la parada. Esto debe estar como reporte en la aplicación de Windows y debe ser posible copiarlo a Excel.
 
Evitar el login de usuario (APP Android). Es preferible solo seleccionar la línea o hacer un login a través de un código de barras. En cuanto a los eventos de línea se debe mantener la cantidad de eventos lo más bajo posible. Cada evento debe contabilizar desde el inicio al final y la causal tomará la última seleccionada (durante el evento se debe poder cambiar el causal)
 
Inclusive estando en parada debería poder empezar una nueva parada. Ej: 
Finaliza jornada, esto es una parada causal:  Fin de jornada. 
 
Al día siguiente la línea no arranca por falta de personal. 
Evento parada, causal: Falta de personal.
 
Todo esto debe manejarse en una interfaz realmente sencilla con botones. El software debe manejarse en tres idiomas intercambiables (español, inglés y creole) 
 
## Fase 1era ##

### Repositorio de datos y control de versión de código (BitBucket )###
* Creación y configuración del repositorio.
* Git para código fuente APP Android (Líneas de producción).
* Git para código fuente APP Admin.
* Git para servicios web.
* Entregable: Credencial administrativa del repositorio.
 
### Diseño Gráfico ###
* Mockups APP Android.
* Mockups APP UWP.
* Elaboracion de vectores y conversión a  imagen (Logos, .PNG, JPG) y paleta de colores  asociado a imagen corporativa.
* Entregables: Archivo .ZIP con todas las imágenes, vectores y mockups elaborados.
 
### Base de datos ###
* Diseño BD.
* Modelado BD. 
* Publicación BD en MS SQL.
* Entregables: diagrama entidad relación, Script BD y credenciales de conexión.
 
 
## Fase 2da ##

### Clases ###
* Diseño de módulo de clases.
* Definición de clases.
* Entregables: código (.ZIP), readme descriptivo métodos y publicación.
 
#### Servicios Web ###
*Diseño de Servicios WEB.
*Publicación de Servicios WEB.
*Entregables: código (.ZIP) y publicación, readme descriptivo de métodos. 
 
### APP Android para líneas de producción ###
* Diseño de Vistas.
* Diseño de Modelos.
* Diseño de Control.
* Pruebas de desarrollo y correcciones. 
* Pruebas Funcionales, FeedBack y correcciones.
* Entregables: código (.ZIP) y publicación.
 
### APP UWP Windows de administración ###
* Diseño de Vistas.
* Diseño de Modelos.
* Diseño de Control.
* Pruebas de desarrollo y correcciones.
* Pruebas funcionales, FeedBack, y correcciones.
* Entregables: código (.ZIP) y publicación
 
### Configuración y publicación en Host ###
* Base de datos. 
* Publicación de  servicios WEB. 
* Publicación de APP Android (.APK) 
* Publicación de APP Admin (UWP).
 
### Manual de usuario APP Android ###
 
### Actualización final de códigos en repositorios ###
 
### Manual de usuario APP Admin ###
